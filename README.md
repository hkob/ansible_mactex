# MacTeX の日本語設定 #

* 日本語の設定が面倒になって来たので、ansible_homebrew からは再度分岐

### MacTeX のインストール後のちょっとした設定 ###

* ヒラギノフォントの設定

### 前提 ###

* MacTeX がインストールされていること
* ansible がインストールされていること (`brew install ansible`)

### 実行方法 ###

* hosts ファイルにマシン一覧を記載します
* シンタックスチェックをします
~~~~
ansible-playbook -i hosts site.yml --syntax-check
~~~~
* Dry-run してみます(管理者の password が必要です)
~~~~
ansible-playbook -i hosts site.yml -K --check
~~~~
* ここまでで問題がなければ実際に実行します(管理者の password が必要です)
~~~~
ansible-playbook -i hosts site.yml -K
~~~~
* 冪等性があるためもう一度実行しても何も起こりません．
* update, noupdate, install, exec, TeXShop などのタグを用意しています。必要なところだけやりたければ -t でタグを設定してください。
